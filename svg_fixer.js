// https://github.com/oslllo/svg-fixer/
const SVGFixer = require('oslllo-svg-fixer');
const fs = require('fs');

const SVGFixerOptions = {
    showProgressBar: true,
    throwIfDestinationDoesNotExist: false,
};

const iconsDir = './icons/';
const concatDir = './concated/';
const fixedDir = './fixed/'

function walkAndFix(dir){
    fs.readdir(dir, (err, files) => {
        files.forEach(file => {
            let path = dir + file + '/';
            let isDir = fs.lstatSync(path).isDirectory();

            if (isDir){
                walkAndFix(path)
            } else if (file.endsWith('.svg')){
                let srcDir = dir + file;
                let newFileName = srcDir.replace(iconsDir, '').split('/').join('-');

                let concatPath = concatDir + newFileName;
                fs.copyFile(srcDir, concatPath, (err) => {
                    if (err){
                        throw err;
                    }
                    SVGFixer(concatPath, fixedDir, SVGFixerOptions).fix()
                    .then(() => {
                        console.log('Done!');
                    })
                    .catch((err) => {
                        console.log('There was an error while processing file: ' + concatPath);
                        throw err;
                    });
                });
            }
        });
    });
}

walkAndFix(iconsDir);
