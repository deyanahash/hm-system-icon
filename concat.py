import os
from xml.dom import minidom

icons_dir = 'icons'
fixed_dir = 'fixed'

excluded = ['flow-hrm-payroll-salary-structure.svg']

if fixed_dir not in os.listdir():
    os.mkdir(fixed_dir)

def removeStroke(el):
    # remove stroke
    if el.hasAttribute('stroke'):
        el.removeAttribute('stroke')
        if not el.hasAttribute('fill'):
            el.setAttribute('fill', 'black')

    # change color to black
    if el.hasAttribute('fill') and el.getAttribute('fill') not in ('none', 'white', '#FFFFFF'):
        el.setAttribute('fill', 'black')
    
    for childEl in el.childNodes:
        if isinstance(childEl, minidom.Element):
            removeStroke(childEl)

for root, dirs, files in os.walk(icons_dir):
    for file in files:
        if not file.endswith('.svg'):
            continue
        path = '-'.join(root.split('\\')[1:])
        new_filename = '%s-%s' % (path, '-'.join(file.split(' ')))

        if new_filename in excluded:
            continue
        src_path = os.path.join(root, file)

        doc = minidom.parse(src_path)
        for svg in doc.getElementsByTagName('svg'):
            removeStroke(svg)

        dest_path = os.path.join(fixed_dir, new_filename)
        with open(dest_path, 'w', encoding='utf-8') as f:
            doc.writexml(f)
